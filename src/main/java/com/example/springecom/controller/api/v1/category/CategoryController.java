package com.example.springecom.controller.api.v1.category;

import com.example.springecom.dto.cart.CartItemDao;
import com.example.springecom.dto.category.CategoryDao;
import com.example.springecom.model.cart.CartItems;
import com.example.springecom.model.category.Category;
import com.example.springecom.service.cart.CartService;
import com.example.springecom.service.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/list")
    private ResponseEntity<List<Category>> getCategories(){
        return ResponseEntity.status(200).body(categoryService.getCategories());
    }

    @PostMapping("/add")
    private ResponseEntity<Category> addCategory(@RequestBody CategoryDao categoryRequest){
        return ResponseEntity.status(200).body(categoryService.addCategory(categoryRequest));
    }
}
