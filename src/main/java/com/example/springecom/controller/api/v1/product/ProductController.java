package com.example.springecom.controller.api.v1.product;

import com.example.springecom.dto.product.ProductDao;
import com.example.springecom.model.product.Product;
import com.example.springecom.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/list")
    private ResponseEntity<List<Product>> getProducts(@RequestParam(name = "category_id") final Long id){
        return ResponseEntity.status(200).body(productService.getProducts(id));
    }

    @PostMapping("/add")
    private ResponseEntity<Product> addProduct(@RequestBody final ProductDao productRequest){
        return ResponseEntity.status(200).body(productService.addProduct(productRequest));
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<String> deleteProduct(@PathVariable("id") final Long id){
        return ResponseEntity.status(200).body(productService.deleteProduct(id));
    }
}
