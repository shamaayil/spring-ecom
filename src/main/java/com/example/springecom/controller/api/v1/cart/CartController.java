package com.example.springecom.controller.api.v1.cart;

import com.example.springecom.dto.cart.CartItemDao;
import com.example.springecom.model.cart.Cart;
import com.example.springecom.model.cart.CartItems;
import com.example.springecom.service.cart.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/cart")
public class CartController {

    @Autowired
    private CartService cardService;

    @GetMapping("/")
    private ResponseEntity<Cart> getCart (@RequestParam(name = "id") Long id){
        return ResponseEntity.status(200).body(cardService.getCart(id));
    }

    @PostMapping("/add")
    private ResponseEntity<CartItems> addToCart (@RequestBody CartItemDao cartItemRequest){
        return ResponseEntity.status(200).body(cardService.addToCart(cartItemRequest));
    }
}
