package com.example.springecom.dto.category;

import lombok.Data;

@Data
public class CategoryDao {
    private String name;
}
