package com.example.springecom.dto.product;

import lombok.Data;

@Data
public class  ProductDao {
    private String name;
    private Float price;
    private Integer count;
    private Long category_id;
}
