package com.example.springecom.dto.cart;

import lombok.Data;

@Data
public class CartItemDao {
    private Long cart_id;
    private Long product_id;
    private Integer product_quantity;
    private String product_amount;
}
