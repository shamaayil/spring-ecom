package com.example.springecom.service.product;

import com.example.springecom.dto.product.ProductDao;
import com.example.springecom.model.product.Product;

import java.util.List;

public interface ProductService {
    List<Product> getProducts(Long id);
    Product addProduct(ProductDao productRequest);
    String deleteProduct(Long id);
}
