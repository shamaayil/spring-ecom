package com.example.springecom.service.product;

import com.example.springecom.dto.product.ProductDao;
import com.example.springecom.model.cart.CartItems;
import com.example.springecom.model.category.Category;
import com.example.springecom.model.product.Product;
import com.example.springecom.repository.category.CategoryRepository;
import com.example.springecom.repository.product.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Product> getProducts(Long id){
        try{
            return (List<Product>) productRepository.findAll();
        }
        catch (Exception e){
            logger.error("Couldn't get products");
            return null;
        }
    }

    @Override
    public Product addProduct(ProductDao productRequest){
        try{
            Category category = categoryRepository.findById(productRequest.getCategory_id()).get();
            Product product = Product.builder()
                    .name(productRequest.getName())
                    .price(productRequest.getPrice())
                    .count(productRequest.getCount())
                    .category(category)
                    .build();
            return productRepository.save(product);
        }
        catch (Exception e){
            System.out.println(e);
            logger.error("Couldn't add product");
            return null;
        }
    }

    @Override
    public String deleteProduct(Long id){
        try{
            Product product = productRepository.findById(id).get();
            productRepository.delete(product);
            return "success";
        }
        catch (Exception e){
            logger.error("Couldn't delete product");
            return "failure";
        }
    }
}
