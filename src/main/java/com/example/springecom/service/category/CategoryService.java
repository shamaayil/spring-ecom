package com.example.springecom.service.category;

import com.example.springecom.dto.category.CategoryDao;
import com.example.springecom.model.category.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getCategories();
    Category addCategory(CategoryDao categoryRequest);
}
