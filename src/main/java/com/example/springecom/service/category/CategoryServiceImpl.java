package com.example.springecom.service.category;

import com.example.springecom.dto.category.CategoryDao;
import com.example.springecom.model.cart.Cart;
import com.example.springecom.model.cart.CartItems;
import com.example.springecom.model.category.Category;
import com.example.springecom.repository.category.CategoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> getCategories(){
        try{
            System.out.println(categoryRepository.findAll());
            return (List<Category>) categoryRepository.findAll();
        }
        catch(Exception e){
            System.out.println(e);
            logger.error("Couldn't get categories");
            return null;
        }
    }

    @Override
    public Category addCategory(CategoryDao categoryRequest){
        try{
            System.out.println(categoryRequest);
            Category category = Category.builder()
                    .name(categoryRequest.getName())
                    .build();
            System.out.println(category);
            return categoryRepository.save(category);
        }
        catch(Exception e){
            logger.error("Couldn't add category");
            return null;
        }
    }
}
