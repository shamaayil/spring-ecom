package com.example.springecom.service.cart;

import com.example.springecom.dto.cart.CartItemDao;
import com.example.springecom.model.cart.Cart;
import com.example.springecom.model.cart.CartItems;

public interface CartService {
    Cart getCart(Long id);
    CartItems addToCart(CartItemDao cartItemRequest);
}
