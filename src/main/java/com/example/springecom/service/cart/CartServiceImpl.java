package com.example.springecom.service.cart;

import com.example.springecom.dto.cart.CartItemDao;
import com.example.springecom.model.cart.Cart;
import com.example.springecom.model.cart.CartItems;
import com.example.springecom.model.product.Product;
import com.example.springecom.repository.cart.CartItemRepository;
import com.example.springecom.repository.cart.CartRepository;
import com.example.springecom.repository.product.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartServiceImpl implements CartService{
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CartItemRepository cartItemRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Cart getCart(Long id){
        try{
            Cart cart = cartRepository.findById(id).get();
//            List<CartItems> cartItems = cart.getCartItems();
//            List<Product> products = cartItems.stream()
//                    .map(CartItems::getProduct)
//                    .toList();
            return cart;
        }
        catch(Exception e){
            logger.error("Couldn't get cart");
            return null;
        }
    }

    @Override
    public CartItems addToCart(CartItemDao cartItemRequest){
        try{
            Product product = productRepository.findById(cartItemRequest.getProduct_id()).get();
            Cart cart = cartRepository.findById(cartItemRequest.getCart_id()).get();
            CartItems cartItem = CartItems.builder()
                    .cart(cart)
                    .product(product)
                    .product_quantity(cartItemRequest.getProduct_quantity())
                    .product_amount(product.getPrice())
                    .build();
            return cartItemRepository.save(cartItem);
        }
        catch(Exception e){
            logger.error("Couldn't add to cart");
            return null;
        }
    }
}
