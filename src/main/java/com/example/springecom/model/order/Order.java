package com.example.springecom.model.order;


import com.example.springecom.model.payment.Payment;
import com.example.springecom.model.product.Product;
import com.example.springecom.model.user.User;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import lombok.Data;

import java.util.List;

@Entity
@Data
@Transactional
@Table(name = "order")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    private String status;
    private Float amount;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private List<OrderItems> order_items;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToOne
    @JoinColumn(name = "payment_id")
    private Payment payment;
}
