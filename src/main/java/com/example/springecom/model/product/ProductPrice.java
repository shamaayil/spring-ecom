package com.example.springecom.model.product;

import com.example.springecom.model.user.User;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import lombok.Data;

@Entity
@Data
@Transactional
@Table(name = "product_price")
public class ProductPrice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    private Float price;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;
}
