package com.example.springecom.model.product;

import com.example.springecom.model.cart.CartItems;
import com.example.springecom.model.category.Category;
import com.example.springecom.model.order.OrderItems;
import com.example.springecom.model.user.User;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import lombok.*;

import java.util.List;

@Entity
@Data
@Builder
@Transactional
@Table(name = "product")
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    private String name;
    private Float price;
    private Integer count;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<OrderItems> order_items;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<CartItems> cart_items;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<ProductPrice> product_prices;
}

