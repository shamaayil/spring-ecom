package com.example.springecom.repository.cart;

import com.example.springecom.model.cart.CartItems;
import org.springframework.data.repository.CrudRepository;

public interface CartItemRepository extends CrudRepository<CartItems, Long> {
}
