package com.example.springecom.repository.cart;

import com.example.springecom.model.cart.Cart;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartRepository extends CrudRepository<Cart, Long> {
    public Optional<Cart> findById(Long id);
}
